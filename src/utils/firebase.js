// utils/firebase.js
import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyCxGUbUSFBeUTAXK4t8ODKL0US4jC29sqA",
    authDomain: "criptoputotes.firebaseapp.com",
    projectId: "criptoputotes",
    storageBucket: "criptoputotes.appspot.com",
    messagingSenderId: "986528916495",
    appId: "1:986528916495:web:0b78cc0fc72e5a759a6f7a"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export { db };
