// pages/records.js

import { useState, useEffect } from 'react';
import { collection, getDocs, query, where } from 'firebase/firestore';
import { db } from '../src/utils/firebase'; 
import '../styles/globals.css';
import Link from 'next/link';

export default function Records() {
  const [records, setRecords] = useState([]);
  const [selectedDate, setSelectedDate] = useState('');
  const [selectedCoin, setSelectedCoin] = useState('');
  const [coinOptions, setCoinOptions] = useState([]);

  useEffect(() => {
    const fetchRecords = async () => {
      const recordsCollection = collection(db, 'records');
      let recordsQuery = query(recordsCollection);
      const coins = new Set();
      
      
      if (selectedDate) {
        recordsQuery = query(recordsCollection, where('date', '==', selectedDate));
      }
      
      if (selectedCoin) {
        recordsQuery = query(recordsCollection, where('coin', '==', selectedCoin));
      }
      
      const snapshot = await getDocs(recordsQuery);

      snapshot.forEach((doc) => {
        const data = doc.data();
        coins.add(data.coin);
      });
      const uniqueCoins = Array.from(coins);
      console.log('Unique coins: ', uniqueCoins);
      setCoinOptions(uniqueCoins);

      const recordsList = snapshot.docs.map((doc) => ({
        id: doc.id,
        ...doc.data(),
      }));
      setRecords(recordsList);
    };

    fetchRecords();
  }, [selectedDate, selectedCoin]);

  const handleDateChange = (e) => {
    setSelectedDate(e.target.value);
  };

  const handleCoinChange = (e) => {
    setSelectedCoin(e.target.value);
  };

  return (
    <div className="container mx-auto content-around">
      <div className="bg-gray-800 p-4 flex justify-between items-center">
        <h1 className="text-white text-xl">Cripto Analysis</h1>
        <Link className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" href="../addRecords">
            <button>Nova análise</button>
        </Link>
      </div>
      <h1 className="text-3xl font-bold mx-4 mb-4 mt-4">Records</h1>
      <div className="mb-4 mx-4">
        <label htmlFor="datePicker" className="block mb-2">Filtrar por data</label>
        <input type="date" id="datePicker" value={selectedDate} onChange={handleDateChange} className="border px-2 py-1" />
      </div>
      <div className="mb-4 mx-4">
        <label htmlFor="coinFilter" className="block mb-2">Filtrar por moeda:</label>
        <select 
          id="coin" 
          value={selectedCoin} 
          onChange={handleCoinChange} 
          className="border px-2 py-1 w-full mb-2"
        >
          <option value="">Select Coin</option>
          {coinOptions.map((coinOption, index) => (
            <option key={index} value={coinOption}>{coinOption}</option>
          ))}
        </select>
      </div>
      <ul className="record-list mx-4">
        {records.map((record) => (
          <li key={record.id} className="record-item">
            <p>Data: {record.date}</p>
            <p>Moeda: {record.coin}</p>
            <p>Análise: {record.analysis}</p>
            {record.imageUrl && <img src={record.imageUrl} alt="Record" className="record-image mt-4 mb-4 mx-auto"  style={{maxWidth: '100%', height: 'auto'}}/>}
          </li>
        ))}
      </ul>
    </div>
  );
}
