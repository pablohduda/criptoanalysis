// pages/addRecord.js

import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { collection, addDoc, getDocs  } from 'firebase/firestore';
import { db } from '../src/utils/firebase'; // Importe a instância do Firebase
import '../styles/globals.css';
import Link from 'next/link';

export default function AddRecord() {
  const [selectedDate, setSelectedDate] = useState('');
  const [analysis, setAnalysis] = useState('');
  const [imageUrl, setImageUrl] = useState('');
  const [coin, setCoin] = useState('');
  const [customCoin, setCustomCoin] = useState('');
  const [coinOptions, setCoinOptions] = useState([]);
  const [showMessage, setShowMessage] = useState(false);

  useEffect(() => {
    const fetchCoinOptions = async () => {
      try {
        const recordsCollection = collection(db, 'records');
        const snapshot = await getDocs(recordsCollection);
        const coins = new Set();

        snapshot.forEach((doc) => {
          const data = doc.data();
          coins.add(data.coin);
        });

        const uniqueCoins = Array.from(coins);
        console.log('Unique coins: ', uniqueCoins);
        setCoinOptions(uniqueCoins);
      } catch (error) {
        console.error('Error fetching coin options: ', error);
      }
    };

    fetchCoinOptions();
  }, []);

  const handleDateChange = (e) => {
    setSelectedDate(e.target.value);
  };

  const handleAnalysisChange = (e) => {
    setAnalysis(e.target.value);
  };

  const handleImageUrlChange = (e) => {
    setImageUrl(e.target.value);
  };

  const handleCoinChange = (e) => {
    const value = e.target.value;
    setCoin(value);
  };

  const handleCustomCoinChange = (e) => {
    setCustomCoin(e.target.value);
  };

  const handleAddCustomCoin = () => {
    setCoin(customCoin);
  };

  const handleSubmit = async () => {
    try {
      const recordsCollection = collection(db, 'records');
      await addDoc(recordsCollection, {
        date: selectedDate,
        analysis: analysis,
        imageUrl: imageUrl,
        coin: coin
      });
      //limpar os campos
      setSelectedDate('');
      setAnalysis('');
      setImageUrl('');
      setCoin('');
      setCustomCoin('');
      setShowMessage(true); // Mostrando a mensagem de sucesso
      setTimeout(() => setShowMessage(false), 5000);
      console.log('Record added successfully!');
    } catch (error) {
      console.error('Error adding record: ', error);
    }
  };

  return (
    <div className="container mx-auto bg-gray-200">
      <div className="bg-gray-800 p-4 flex justify-between items-center">
      <h1 className="text-white text-xl">Cripto Analysis</h1>
      <Link href="/records">
        <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Análises</button>
      </Link>
    </div>
      {showMessage && (
        <div className="bg-green-200 text-green-800 p-4 mb-4 mx-4">
          Record added successfully!
        </div>
      )}
      <h1 className="text-3xl font-bold mb-4"/>
      <h1 className="text-3xl font-bold mb-4 mt-4 mx-4">Add Record</h1>
      <div className="mb-4">
        <label htmlFor="datePicker" className="block mb-2 mx-4">Date:</label>
        <input 
          type="date" 
          id="datePicker" 
          value={selectedDate} 
          onChange={handleDateChange} 
          className="border px-2 py-1 mb-2 mx-4 bg-gray-50" 
        />
        <label htmlFor="analysis" className="block mb-2 mx-4">Analysis:</label>
        <input 
          type="text" 
          id="analysis" 
          value={analysis} 
          onChange={handleAnalysisChange} 
          className="border px-2 py-1 w-5/6 mb-2 mx-4" 
        />
        <label htmlFor="imageUrl" className="block mb-2 mx-4">Image URL:</label>
        <input 
          type="text" 
          id="imageUrl" 
          value={imageUrl} 
          onChange={handleImageUrlChange} 
          className="border px-2 py-1 w-5/6 mb-2 mx-4" 
        />
        <label htmlFor="coin" className="block mb-2 mx-4">Coin:</label>
        <select 
          id="coin" 
          value={customCoin || coin} 
          onChange={handleCoinChange} 
          className="border px-2 py-1 w-5/6 mb-2 mx-4"
        >
          <option value="">Select Coin</option>
          {coinOptions.map((coinOption, index) => (
            <option key={index} value={coinOption}>{coinOption}</option>
          ))}
          {customCoin && <option value={customCoin}>{customCoin}</option>}
          <option value="other">Other</option>
        </select>
        {coin === 'other' && (
          <div className="mt-2">
            <input 
              type="text" 
              placeholder="Enter custom coin" 
              value={customCoin} 
              onChange={handleCustomCoinChange} 
              className="border px-2 py-1 w-5/6 mx-4 mb-2" 
            />
            <button onClick={handleAddCustomCoin} className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
              Add Custom Coin
            </button>
          </div>
        )}
      </div>
      <button onClick={handleSubmit} className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mx-4">
        Submit
      </button>
    </div>
  );
}